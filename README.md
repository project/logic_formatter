CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Logic formatter module provide field formatters for next core field types:

* entity_reference,
* list_integer,
* list_float,
* list_string

Formatter apply logic operation to field values (keys for list_*, ids for entity_reference)
and output plain text or markup based on boolean result.


### Operations supported

* Any IN,
* Any NOT IN,
* All IN,
* All NOT IN,
* EXPR

First four operations just test field values against space separated list of values specified by _operand_ setting.

_EXPR_ calculate specific expression specified by _operand_ setting.


### EXPR operation expression examples

Space separated lists of values for exact match or include/exclude. Note: field values compares as strings.

`A B C`: Exact match. True then field contains 'A' and 'B' and 'C' and nothing else.

` `: True then field contains nothing.

`A B ! C D`: Include/exclude. True then field include 'A' and 'B' and not include 'C' or 'D'.

`A B C !`: True then field include 'A' and 'B' and 'C' and anything else.

`! A B C`: True then field not include 'A' or 'B' or 'C'.

The char '^' in *first position* of values list specify result negation.

`^ A B C`: Negation of exact match. True then not (field contains only 'A' and 'B' and 'C').

`^`: True then field not empty.

Logic alternatives can be specified with '|' char.

`A | B C | D E ! F`: Thue then (contains only 'A') OR (contains only 'B' and 'C') OR (include 'D' and 'E' but not 'F').

`|A!`: True then field empty or include 'A'.

`A!|^B!C`: True then field (include 'A') or (not(include 'B' and not include 'C')).


### More info

For a full description of the module, visit the project page:
   https://www.drupal.org/project/tag_formatter

To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/tag_formatter


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the logic_formatter module as you would normally install a contributed
 Drupal module. Visit https://www.drupal.org/node/1897420 for further 
 information.


MAINTAINERS
-----------

 * Nikita Shishatsky (altside.ru) - [https://www.drupal.org/u/altsideru](https://www.drupal.org/u/altsideru)
