<?php

namespace Drupal\logic_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'logic_formatter_options_key' formatter.
 * 
 * @FieldFormatter(
 *  id = "logic_formatter_options_key",
 *  label = @Translation("Logic Formatter"),
 *  description = @Translation("Conditional logic for output."),
 *  field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *  }
 * )
 */
class OptionsKeyLogicFormatter extends FormatterBase {

    /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'operation' => 'any_in',
      'operand' => '',
      'out0' => '0',
      'out1' => '1',
      'out_fmt' => 'plain_text',
    ] + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['operation'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Operation'),
        '#description'   => $this->t('Operation on list keys.'),
        '#default_value' => $this->getSetting('operation'),
        '#options'       => [
            'any_in'   => $this->t('Any IN'),
            'any_not_in'  => 'Any NOT IN',
            'all_in'   => 'All IN',
            'all_not_in'  => 'All NOT IN',
            'expr' => 'EXPR'
            ],
        ];
    $element['operand'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Operand'),
        '#description'   => $this->t('Space separated list of ids. <br>Expression for EXPR operation.'),
        '#default_value' => $this->getSetting('operand'),
        '#size' => 40,
        ];
    $element['out1'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('True'),
        '#description'   => $this->t('Output for true condition.'),
        '#default_value' => $this->getSetting('out1'),
        '#size' => 40,
        ];
    $element['out0'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('False'),
        '#description'   => $this->t('Output for false condition.'),
        '#default_value' => $this->getSetting('out0'),
        '#size' => 40,
        ];
    $element['out_fmt'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Output format'),
        '#default_value' => $this->getSetting('out_fmt'),
        '#options'       => [
            'plain_text'   => $this->t('Plain text'),
            'markup'  => $this->t('Markup'),
            ],
        ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->getSetting('operation') . ' ( ' . $this->getSetting('operand') . ' )';
    $summary[] = $this->getSetting('out_fmt');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $vals = [];
    foreach ($items as $delta => $item) $vals[] = $item->value;

    $r = \Drupal::service('logic_formatter.expr')
      ->calc(
          $this->getSetting('operation'), 
          $this->getSetting('operand'), 
          $vals
        );

    $key = $this->getSetting('out_fmt');
    if ($key == 'markup') $key = '#markup';
    else $key = '#plain_text';

    return [[
      $key => (($r)
        ? ($this->getSetting('out1')) 
        : ($this->getSetting('out0')))
    ]];

  }

}
