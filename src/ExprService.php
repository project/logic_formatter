<?php
/**
 * @file
 * Contains \Drupal\logic_formatter\ExprService service
 * get api: \Drupal::service('logic_formatter.expr');
 * 
 */

namespace Drupal\logic_formatter;

class ExprService {

  /** 
   * Split $str to array of words.
   */ 
  public static function words($str) {
    $w = explode(' ', $str);
    $out = [];
    foreach ($w as $s) if (!empty($s)) $out[] = $s;
    return $out;
  }

  /** 
   * Calculate boolean for expression 
   * $expr: (string), 'keys only | contains ! not cont |^ negated_and_expr'
   * $vals: (array) values to check
   * return boolean
   */ 
  public static function expr($expr, $vals){
    $ors = explode('|', $expr);
    foreach ($ors as $alt) {
      $neg = false;
      if ($alt[0] === '^') {
        $neg=true;
        $alt = substr($alt,1);
      }
      $part = explode('!', $alt, 2);
      $p = self::words($part[0]);
      if (count($part) < 2) {
        $r = empty(array_diff($p,$vals)) 
          && empty(array_diff($vals,$p));
        if ($neg) $r = !$r;
        if ($r) return true;
        continue;
      }
      $r = empty(array_diff($p,$vals)) 
        && empty(array_intersect($vals, self::words($part[1])));
      if ($neg) $r = !$r;
      if ($r) return true;
    }
    return false;
  }
  
  /**
   * Calculate boolean for operation
   * $op: (string) operation, one of 'all_in', 'all_not_in', 'any_in', 'any_not_in', 'expr'
   * $expr: (string) list of ids; or expression for $op == 'expr'
   * $vals: (array) values to check
   * return boolean
   */
  public static function calc($op, $expr, $vals) {
    if ($op === 'expr') return self::expr($expr,$vals);
    $w = self::words($expr);
    switch ($op) {
      case 'any_in': return !empty(array_intersect($vals,$w));
      case 'any_not_in': return !empty(array_diff($vals,$w));
      case 'all_in': return empty(array_diff($vals,$w));
      case 'all_not_in': return empty(array_intersect($vals,$w));
    }
    return false;
  }

}
